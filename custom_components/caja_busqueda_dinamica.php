<?php

namespace WPC;

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}


class CajaBusquedaDinamica extends \Elementor\Widget_Base
{

  public function __construct($data = [], $args = null) {
    parent::__construct($data, $args);
  }

  public function get_name()
  {
    return 'caja-busqueda-dinamica';
  }

  public function get_title()
  {
    return 'Caja busqueda dinámica';
  }
 
  public function get_icon() {
		return 'eicon-icon-box';
	}

  public function get_categories()
  {
    return ['basic'];
  }

  protected function _register_controls() {

    $this->start_controls_section('hotel_selection', [
        'label' => 'Caja de busqueda',
        'tab' => \Elementor\Controls_Manager::TAB_CONTENT
    ]);

    $this->add_control('titulo',
      [
        'type' => \Elementor\Controls_Manager::TEXT,
        'label' => 'Titulo',
        'placeholder' => 'titulo',
        'dynamic' => [
          'active' => true,
        ],
      ]
    );

    $this->add_control('title_header',
      [
        'type' => \Elementor\Controls_Manager::TEXT,
        'label' => 'Titulo Principal',
        'placeholder' => 'Titulo Principal',
        'dynamic' => [
          'active' => true,
        ],
      ]
    );


    $this->add_control('second_title',
      [
        'type' => \Elementor\Controls_Manager::TEXT,
        'label' => 'Sub Titulo',
        'placeholder' => 'Sub titulo',
        'dynamic' => [
          'active' => true,
        ],
      ]
    );

    //selecciona_una_oferta_
$this->add_control('opcion_oferta',
[
  'type' => \Elementor\Controls_Manager::TEXT,
  'label' => 'Oferta seleccionada',
  'placeholder' => 'Oferta seleccionada',
  'dynamic' => [
    'active' => true,
  ],
]
);
//contdown logo
$this->add_control('logo_campaign',
 [
   'label' => 'Logo Campaign',
   'type' => \Elementor\Controls_Manager::MEDIA,
   'default' => [
     'url' => \Elementor\Utils::get_placeholder_image_src(),
   ],
   'dynamic' => [
     'active' => true,
   ],
 ]
);

$this->add_control('contador_desde',
 [
   'type' => \Elementor\Controls_Manager::TEXT,
   'label' => 'Contador desde',
   'placeholder' => '2024-10-01',
   'dynamic' => [
     'active' => true,
   ],
 ]
);
//--
$this->add_control('hora_desde',
 [
   'type' => \Elementor\Controls_Manager::TEXT,
   'label' => 'Hora desde',
   'placeholder' => '00:00',
   'dynamic' => [
     'active' => true,
   ],
 ]
);
//--
$this->add_control('contador_hasta',
 [
   'type' => \Elementor\Controls_Manager::TEXT,
   'label' => 'Contador hasta',
   'placeholder' => '2024-10-30',
   'dynamic' => [
     'active' => true,
   ],
 ]
);
//--
$this->add_control('hora_hasta',
 [
   'type' => \Elementor\Controls_Manager::TEXT,
   'label' => 'Hora hasta',
   'placeholder' => '00:00',
   'dynamic' => [
     'active' => true,
   ],
 ]
);

 //countdown

    $this->add_control('bg',
      [
        'label' => 'Imagen de fondo',
        'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
        'dynamic' => [
          'active' => true,
        ],
      ]
    );

    $this->add_control('icono1',
      [
        'label' => 'Icono 1',
        'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
        'dynamic' => [
          'active' => true,
        ],
      ]
    );

    $this->add_control('icono2',
      [
        'label' => 'Icono 2',
        'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
        'dynamic' => [
          'active' => true,
        ],
      ]
    );

    $this->add_control('codigo',[
        'type' => \Elementor\Controls_Manager::TEXTAREA,
        'label' => 'Codigo',
        'placeholder' => 'codigo'
      ]
    );

    $this->end_controls_section();
  }

  protected function render() {
    
    $settings = $this->get_settings_for_display();
    if(empty($settings['opcion_oferta'][0])){
      $settings['opcion_oferta'][0]='0';
    }
    //echo var_dump($settings);
  ?>
  <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
  <script src="https://www.pricetravel.com/ofertas-de-viajes/wp-content/plugins/go1mx-dynamic-landing/js/lightpick.min.js?v=1"></script>
  <link rel="stylesheet" href="https://www.pricetravel.com/ofertas-de-viajes/wp-content/plugins/go1mx-dynamic-landing/css/lightpick.css?v=2">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <style>


/*213 contdown css*/
.wmk-bc-header {
    padding-bottom: 15px !important;
    padding-left: 15px !important;
    padding-right: 15px !important;
}

@media (min-width: 768px) {
    .c-countdown {
        position: absolute;
        right: 0;
        margin-right: 95px !important;
         top: 55px;
         
    }
}

@media (min-width: 1280px) {
    .c-countdown {
        position: absolute;
        right: 0;
        margin-right: 10px !important;
        top: 30px;
        
    }
}


@media (min-width: 768px) {
    .p-order-1 {
        order: 1
    }
    .p-order-2 {
        order: 2
    }
    .p-order-3 {
        order: 3
    }    
}

@media (max-width: 767px) {
    .p-order-1 {
        order: 1
    }
    .p-order-2 {
        order: 3
    }
    .p-order-3 {
        order: 2
    }
    .c-countdown {
        margin-top: 20px !important;
        max-width: 100% !important;
    }
}

/*fin countdown css */


    .wmk-bc-header{
      background-color: transparent;
      background-image: linear-gradient(150deg, #0193D4 0%, #D116A8 79%);
    }
    .sp-03{
      font-size:20px;
      padding: 0;
      display:inline-block;
    }
    .wmk-bc-header{font-family:-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen,Ubuntu,Cantarell,Open Sans,Helvetica Neue,sans-serif;position:relative;padding-bottom:24px}.wmk-bc-header .wmk-bc-img{height:100%;line-height:1.2;position:relative;z-index:1}.wmk-bc-header .wmk-bc-img h4{color:#fff;font-size:30px;font-weight:700;margin:20px 0 10px}@media (max-width:767px){.wmk-bc-header .wmk-bc-img h4{font-size:20px;margin-bottom:0}}@media (max-width:767px){.wmk-bc-header .wmk-bc-img{height:auto}}.wmk-bc-header .wmk-bc-img .sp-03{color:#fff;position:relative;top:70px}@media (max-width:767px),(min-width:768px) and (max-width:1024px){.wmk-bc-header .wmk-bc-img .sp-03{float:right;top:40px}}@media (max-width:767px){.wmk-bc-header .wmk-bc-img .sp-03 span{display:block}}.wmk-bc-header .wmk-bc-img .sp-03 img{width:25px;margin:0 5px;position:relative;top:7px}@media (max-width:767px){.wmk-bc-header .wmk-bc-img .sp-03 img{top:0}}.wmk-bc-header .wmk-bc-img .c-sp{color:#fff}@media (max-width:767px){.wmk-bc-header .wmk-bc-img .c-sp span{display:block}}.wmk-bc-header .wmk-bc-img .c-sp .sp-01{font-size:16px}.wmk-bc-header .wmk-bc-img .c-sp .sp-02{font-size:50px;font-weight:700}@media (max-width:767px){.wmk-bc-header .wmk-bc-img .c-sp .sp-02{font-size:24px}}.wmk-bc-header .wmk-bc-img .c-sp p{margin:0 0 10px;padding-left:50px}@media (max-width:767px){.wmk-bc-header .wmk-bc-img .c-sp p{padding-left:0;margin-bottom:0}}.wmk-bc-header .wmk-bc-img h5{display:block;font-weight:600;margin:20px 0}@media (max-width:767px){.wmk-bc-header .wmk-bc-img h5{color:#fff!important;margin:10px 0;font-size:20px}}@media (min-width:1025px){.wmk-bc-header .wmk-bc-img h5{width:200px;text-align:left}.wmk-bc-header .wmk-bc-img h5 span{display:block}}.wmk-bc-header .wmk-img-bg{height:100%}@media (max-width:767px){.wmk-bc-header .wmk-img-bg{height:auto}}.wmk-bc-header .wmk-bc-i-mask{bottom:0;-webkit-clip-path:ellipse(62% 82% at 65% 36%);clip-path:ellipse(62% 82% at 65% 36%);position:absolute;top:0;right:0;width:50%}@media (max-width:767px){.wmk-bc-header .wmk-bc-i-mask{display:none}}
.wmk-img-bg {
    object-fit: cover;
}



    *,:after,:before{box-sizing:border-box}.bpt-container{font-family:roboto}.bpt-tab{overflow:hidden}.bpt-tab a,.bpt-tab button{background-color:#51408e;border-radius:10px 10px 0 0;color:#fff;margin-right:5px;float:left;border:none;outline:none;cursor:pointer;padding:14px 12px;transition:.3s;font-size:14px;text-decoration:none}.bpt-tab button:hover{background-color:#ddd}.bpt-tab button.bpt-active{color:#51408e}.bpt-tab button.bpt-active,.bpt-tabcontent{background-color:#fff;box-shadow:0 .5rem 1rem rgba(0,0,0,.15)!important;border:1px solid #ddd}.bpt-tabcontent{display:none;padding:6px 12px;margin-top:-4px;position:relative;z-index:2}.b-label,.bpt-d-block{display:block}.b-label{color:#2d2d2d;font-size:14px;margin-bottom:5px}.b-input{display:block;border:none!important;border-radius:0!important;border-bottom:1px solid #aba8a9!important;font-size:16px;height:40px;padding-left:35px!important;width:100%!important}.b-input:focus{outline:none}.b-input:placeholder{color:#2d2d2d}.b-form{padding:20px 0;position:relative}@media (max-width:767px){.b-form{padding:10px 0}}.b-form .b-icon{color:#51408e;left:5px;position:absolute;top:46px}@media (max-width:767px){.b-form .b-icon{top:36px}}.b-simple-icon{color:#51408e}.b-btn{position:relative;top:40px;display:inline-block;font-weight:400;color:#212529;text-align:center;vertical-align:middle;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;background-color:transparent;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;width:100%}@media (max-width:767px){.b-btn{top:0}}.b-btn:hover{color:#212529;text-decoration:none}.b-btn.focus,.b-btn:focus{outline:0;box-shadow:0 0 0 .2rem rgba(0,123,255,.25)}.b-btn.disabled,.b-btn:disabled{opacity:.65}a.b-btn.disabled,fieldset:disabled a.b-btn{pointer-events:none}.b-btn-primary{color:#fff;background-color:#e50468;border-color:#e50468;transition:all .2s linear}.b-btn-primary:hover{background-color:#ce035d;border-color:#ce035d;color:#fff;cursor:pointer}.b-container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}@media (min-width:576px){.b-container{max-width:540px}}@media (min-width:768px){.b-container{max-width:720px}}@media (min-width:992px){.b-container{max-width:960px}}@media (min-width:1200px){.b-container{max-width:1140px}}.b-container-fluid{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.b-row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.b-col,.b-col-1,.b-col-2,.b-col-3,.b-col-4,.b-col-5,.b-col-6,.b-col-7,.b-col-8,.b-col-9,.b-col-10,.b-col-11,.b-col-12,.b-col-auto,.b-col-lg,.b-col-lg-1,.b-col-lg-2,.b-col-lg-3,.b-col-lg-4,.b-col-lg-5,.b-col-lg-6,.b-col-lg-7,.b-col-lg-8,.b-col-lg-9,.b-col-lg-10,.b-col-lg-11,.b-col-lg-12,.b-col-lg-auto,.b-col-md,.b-col-md-1,.b-col-md-2,.b-col-md-3,.b-col-md-4,.b-col-md-5,.b-col-md-6,.b-col-md-7,.b-col-md-8,.b-col-md-9,.b-col-md-10,.b-col-md-11,.b-col-md-12,.b-col-md-auto,.b-col-sm,.b-col-sm-1,.b-col-sm-2,.b-col-sm-3,.b-col-sm-4,.b-col-sm-5,.b-col-sm-6,.b-col-sm-7,.b-col-sm-8,.b-col-sm-9,.b-col-sm-10,.b-col-sm-11,.b-col-sm-12,.b-col-sm-auto,.b-col-xl,.b-col-xl-1,.b-col-xl-2,.b-col-xl-3,.b-col-xl-4,.b-col-xl-5,.b-col-xl-6,.b-col-xl-7,.b-col-xl-8,.b-col-xl-9,.b-col-xl-10,.b-col-xl-11,.b-col-xl-12,.b-col-xl-auto{position:relative;width:100%;padding-right:15px;padding-left:15px}.b-col{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.b-col-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}.b-col-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.b-col-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.b-col-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.b-col-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.b-col-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.b-col-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.b-col-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.b-col-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.b-col-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.b-col-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.b-col-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.b-col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.b-order-first{-ms-flex-order:-1;order:-1}.b-order-last{-ms-flex-order:13;order:13}.b-order-0{-ms-flex-order:0;order:0}.b-order-1{-ms-flex-order:1;order:1}.b-order-2{-ms-flex-order:2;order:2}.b-order-3{-ms-flex-order:3;order:3}.b-order-4{-ms-flex-order:4;order:4}.b-order-5{-ms-flex-order:5;order:5}.b-order-6{-ms-flex-order:6;order:6}.b-order-7{-ms-flex-order:7;order:7}.b-order-8{-ms-flex-order:8;order:8}.b-order-9{-ms-flex-order:9;order:9}.b-order-10{-ms-flex-order:10;order:10}.b-order-11{-ms-flex-order:11;order:11}.b-order-12{-ms-flex-order:12;order:12}.b-offset-1{margin-left:8.333333%}.b-offset-2{margin-left:16.666667%}.b-offset-3{margin-left:25%}.b-offset-4{margin-left:33.333333%}.b-offset-5{margin-left:41.666667%}.b-offset-6{margin-left:50%}.b-offset-7{margin-left:58.333333%}.b-offset-8{margin-left:66.666667%}.b-offset-9{margin-left:75%}.b-offset-10{margin-left:83.333333%}.b-offset-11{margin-left:91.666667%}@media (min-width:576px){.b-col-sm{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.b-col-sm-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}.b-col-sm-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.b-col-sm-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.b-col-sm-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.b-col-sm-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.b-col-sm-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.b-col-sm-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.b-col-sm-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.b-col-sm-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.b-col-sm-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.b-col-sm-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.b-col-sm-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.b-col-sm-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.b-order-sm-first{-ms-flex-order:-1;order:-1}.b-order-sm-last{-ms-flex-order:13;order:13}.b-order-sm-0{-ms-flex-order:0;order:0}.b-order-sm-1{-ms-flex-order:1;order:1}.b-order-sm-2{-ms-flex-order:2;order:2}.b-order-sm-3{-ms-flex-order:3;order:3}.b-order-sm-4{-ms-flex-order:4;order:4}.b-order-sm-5{-ms-flex-order:5;order:5}.b-order-sm-6{-ms-flex-order:6;order:6}.b-order-sm-7{-ms-flex-order:7;order:7}.b-order-sm-8{-ms-flex-order:8;order:8}.b-order-sm-9{-ms-flex-order:9;order:9}.b-order-sm-10{-ms-flex-order:10;order:10}.b-order-sm-11{-ms-flex-order:11;order:11}.b-order-sm-12{-ms-flex-order:12;order:12}.b-offset-sm-0{margin-left:0}.b-offset-sm-1{margin-left:8.333333%}.b-offset-sm-2{margin-left:16.666667%}.b-offset-sm-3{margin-left:25%}.b-offset-sm-4{margin-left:33.333333%}.b-offset-sm-5{margin-left:41.666667%}.b-offset-sm-6{margin-left:50%}.b-offset-sm-7{margin-left:58.333333%}.b-offset-sm-8{margin-left:66.666667%}.b-offset-sm-9{margin-left:75%}.b-offset-sm-10{margin-left:83.333333%}.b-offset-sm-11{margin-left:91.666667%}}@media (min-width:768px){.b-col-md{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.b-col-md-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}.b-col-md-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.b-col-md-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.b-col-md-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.b-col-md-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.b-col-md-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.b-col-md-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.b-col-md-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.b-col-md-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.b-col-md-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.b-col-md-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.b-col-md-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.b-col-md-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.b-order-md-first{-ms-flex-order:-1;order:-1}.b-order-md-last{-ms-flex-order:13;order:13}.b-order-md-0{-ms-flex-order:0;order:0}.b-order-md-1{-ms-flex-order:1;order:1}.b-order-md-2{-ms-flex-order:2;order:2}.b-order-md-3{-ms-flex-order:3;order:3}.b-order-md-4{-ms-flex-order:4;order:4}.b-order-md-5{-ms-flex-order:5;order:5}.b-order-md-6{-ms-flex-order:6;order:6}.b-order-md-7{-ms-flex-order:7;order:7}.b-order-md-8{-ms-flex-order:8;order:8}.b-order-md-9{-ms-flex-order:9;order:9}.b-order-md-10{-ms-flex-order:10;order:10}.b-order-md-11{-ms-flex-order:11;order:11}.b-order-md-12{-ms-flex-order:12;order:12}.b-offset-md-0{margin-left:0}.b-offset-md-1{margin-left:8.333333%}.b-offset-md-2{margin-left:16.666667%}.b-offset-md-3{margin-left:25%}.b-offset-md-4{margin-left:33.333333%}.b-offset-md-5{margin-left:41.666667%}.b-offset-md-6{margin-left:50%}.b-offset-md-7{margin-left:58.333333%}.b-offset-md-8{margin-left:66.666667%}.b-offset-md-9{margin-left:75%}.b-offset-md-10{margin-left:83.333333%}.b-offset-md-11{margin-left:91.666667%}}@media (min-width:992px){.b-col-lg{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.b-col-lg-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}.b-col-lg-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.b-col-lg-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.b-col-lg-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.b-col-lg-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.b-col-lg-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.b-col-lg-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.b-col-lg-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.b-col-lg-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.b-col-lg-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.b-col-lg-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.b-col-lg-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.b-col-lg-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.b-order-lg-first{-ms-flex-order:-1;order:-1}.b-order-lg-last{-ms-flex-order:13;order:13}.b-order-lg-0{-ms-flex-order:0;order:0}.b-order-lg-1{-ms-flex-order:1;order:1}.b-order-lg-2{-ms-flex-order:2;order:2}.b-order-lg-3{-ms-flex-order:3;order:3}.b-order-lg-4{-ms-flex-order:4;order:4}.b-order-lg-5{-ms-flex-order:5;order:5}.b-order-lg-6{-ms-flex-order:6;order:6}.b-order-lg-7{-ms-flex-order:7;order:7}.b-order-lg-8{-ms-flex-order:8;order:8}.b-order-lg-9{-ms-flex-order:9;order:9}.b-order-lg-10{-ms-flex-order:10;order:10}.b-order-lg-11{-ms-flex-order:11;order:11}.b-order-lg-12{-ms-flex-order:12;order:12}.b-offset-lg-0{margin-left:0}.b-offset-lg-1{margin-left:8.333333%}.b-offset-lg-2{margin-left:16.666667%}.b-offset-lg-3{margin-left:25%}.b-offset-lg-4{margin-left:33.333333%}.b-offset-lg-5{margin-left:41.666667%}.b-offset-lg-6{margin-left:50%}.b-offset-lg-7{margin-left:58.333333%}.b-offset-lg-8{margin-left:66.666667%}.b-offset-lg-9{margin-left:75%}.b-offset-lg-10{margin-left:83.333333%}.b-offset-lg-11{margin-left:91.666667%}}@media (min-width:1200px){.b-col-xl{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}.b-col-xl-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}.b-col-xl-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.b-col-xl-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.b-col-xl-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.b-col-xl-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.b-col-xl-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.b-col-xl-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.b-col-xl-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.b-col-xl-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.b-col-xl-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.b-col-xl-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.b-col-xl-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.b-col-xl-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.b-order-xl-first{-ms-flex-order:-1;order:-1}.b-order-xl-last{-ms-flex-order:13;order:13}.b-order-xl-0{-ms-flex-order:0;order:0}.b-order-xl-1{-ms-flex-order:1;order:1}.b-order-xl-2{-ms-flex-order:2;order:2}.b-order-xl-3{-ms-flex-order:3;order:3}.b-order-xl-4{-ms-flex-order:4;order:4}.b-order-xl-5{-ms-flex-order:5;order:5}.b-order-xl-6{-ms-flex-order:6;order:6}.b-order-xl-7{-ms-flex-order:7;order:7}.b-order-xl-8{-ms-flex-order:8;order:8}.b-order-xl-9{-ms-flex-order:9;order:9}.b-order-xl-10{-ms-flex-order:10;order:10}.b-order-xl-11{-ms-flex-order:11;order:11}.b-order-xl-12{-ms-flex-order:12;order:12}.b-offset-xl-0{margin-left:0}.b-offset-xl-1{margin-left:8.333333%}.b-offset-xl-2{margin-left:16.666667%}.b-offset-xl-3{margin-left:25%}.b-offset-xl-4{margin-left:33.333333%}.b-offset-xl-5{margin-left:41.666667%}.b-offset-xl-6{margin-left:50%}.b-offset-xl-7{margin-left:58.333333%}.b-offset-xl-8{margin-left:66.666667%}.b-offset-xl-9{margin-left:75%}.b-offset-xl-10{margin-left:83.333333%}.b-offset-xl-11{margin-left:91.666667%}}.bpt-container{position:relative;z-index:6!important}.c-datepicker .lightpick.is-hidden{position:relative;z-index:0!important}.c-datepicker.z-datepicker{position:relative;z-index:7}@media (max-width:767px){.c-datepicker .lightpick{position:fixed!important;top:0!important;right:0!important;bottom:0!important;left:0!important}}.b-d-none{display:none!important}.b-d-block{display:block!important}.b-w-100{width:100%!important}.b-shadow{box-shadow:0 .5rem 1rem rgba(0,0,0,.15)!important}.b-p-0{padding:0}.b-text-center{text-align:center}.b-mb-3{margin-bottom:1rem!important}.b-mb-4{margin-bottom:1.5rem!important}.b-mt-3{margin-top:1rem!important}.b-form-group{margin-bottom:1rem}.b-form-group label{font-size:14px;margin-top:5px}label{display:inline-block;margin-bottom:.5rem}.b-form-control{display:block;width:100%;height:calc(1.5em + .75rem + 2px);padding:.375rem .75rem;font-size:1rem;font-weight:400;line-height:1.5;color:#495057;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;border-radius:.25rem;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out}@media (min-width:768px) and (max-width:1024px),(min-width:1025px){.b-hr{border:1px solid #eee;margin-bottom:10px}}.bpt-dropdown{background-color:#fff;padding:15px;width:250px;position:absolute;z-index:4}@media (max-width:767px){.bpt-dropdown .bpt-d-int{margin:30px auto 0;width:90%}}@media (max-width:767px){.bpt-dropdown{position:fixed;top:0;right:0;bottom:0;z-index:5;background:#fff;left:0;width:100%}}.bpt-dropdown .b-c-title{font-size:20px}.bpt-dropdown .b-d-header{padding-bottom:15px;display:block}@media (max-width:767px){.bpt-dropdown .b-d-header{border-bottom:1px solid #eee;margin-bottom:25px}}.bpt-dropdown .b-d-header .material-icons{cursor:pointer;font-size:16px;float:right}.bpt-dropdown .b-btn-circle{background-color:#fff;border-radius:50px;box-shadow:0 .5rem 1rem rgba(0,0,0,.15)!important;cursor:pointer;padding:0;width:28px;height:28px}.bpt-dropdown .b-btn-circle .b-simple-icon{display:block;text-align:center;position:relative;top:2px}.bpt-dropdown .b-btn-circle:hover{box-shadow:0 .5rem 1rem rgba(0,0,0,.25)!important}.bpt-dropdown .b-result{left:10px;position:relative;top:5px}.bpt-dropdown .b-c-minor{padding:0 15px}.bpt-dropdown .b-simple-btn{background-color:#f8f9fa;border:none;border-radius:4px;color:#51408e;margin:15px auto;padding:10px 15px;text-align:center;transition:all .2s linear;width:90%}.bpt-dropdown .b-simple-btn:hover{cursor:pointer;background-color:#e2e6ea}.bpt-dropdown .b-btn{margin:auto;top:0;width:90%}small{font-size:10px}.show{display:block}.hide{display:none}.container-fluid{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.search-results,.search-results_f_d,.search-results_f_o,.search-results_p_d,.search-results_p_o{position:absolute;width:100%;z-index:1;top:40px;min-width:850px;margin-top:40px}.search-results .mobile-element,.search-results_f_d .mobile-element,.search-results_f_o .mobile-element,.search-results_p_d .mobile-element,.search-results_p_o .mobile-element{display:none}@media (max-width:767px){.search-results,.search-results_f_d,.search-results_f_o,.search-results_p_d,.search-results_p_o{position:fixed;top:0;left:0;width:100%;height:100%;background:#fff;z-index:3;min-width:100%;margin-top:0}.search-results .mobile-element,.search-results_f_d .mobile-element,.search-results_f_o .mobile-element,.search-results_p_d .mobile-element,.search-results_p_o .mobile-element{display:block;padding-left:30px;padding-right:30px}.search-results .mobile-scroll,.search-results_f_d .mobile-scroll,.search-results_f_o .mobile-scroll,.search-results_p_d .mobile-scroll,.search-results_p_o .mobile-scroll{overflow-x:scroll;height:720px;padding-bottom:20px;width:100%}.search-results .mobile-scroll .autocomplete-box,.search-results_f_d .mobile-scroll .autocomplete-box,.search-results_f_o .mobile-scroll .autocomplete-box,.search-results_p_d .mobile-scroll .autocomplete-box,.search-results_p_o .mobile-scroll .autocomplete-box{width:100%}.search-results .mobile-scroll .autocomplete-counter,.search-results_f_d .mobile-scroll .autocomplete-counter,.search-results_f_o .mobile-scroll .autocomplete-counter,.search-results_p_d .mobile-scroll .autocomplete-counter,.search-results_p_o .mobile-scroll .autocomplete-counter{display:none}.search-results .mobile-header,.search-results_f_d .mobile-header,.search-results_f_o .mobile-header,.search-results_p_d .mobile-header,.search-results_p_o .mobile-header{border-bottom:1px solid #d5d4d4}.search-results .mobile-header .align-items-center,.search-results_f_d .mobile-header .align-items-center,.search-results_f_o .mobile-header .align-items-center,.search-results_p_d .mobile-header .align-items-center,.search-results_p_o .mobile-header .align-items-center{-ms-flex-align:center!important;align-items:center!important}.search-results .mobile-header .justify-content-between,.search-results_f_d .mobile-header .justify-content-between,.search-results_f_o .mobile-header .justify-content-between,.search-results_p_d .mobile-header .justify-content-between,.search-results_p_o .mobile-header .justify-content-between{-ms-flex-pack:justify!important;justify-content:space-between!important}.search-results .mobile-header .d-flex,.search-results_f_d .mobile-header .d-flex,.search-results_f_o .mobile-header .d-flex,.search-results_p_d .mobile-header .d-flex,.search-results_p_o .mobile-header .d-flex{display:-ms-flexbox!important;display:flex!important}.search-results .mobile-header .btn-link,.search-results_f_d .mobile-header .btn-link,.search-results_f_o .mobile-header .btn-link,.search-results_p_d .mobile-header .btn-link,.search-results_p_o .mobile-header .btn-link{color:#5c469c}.search-results_f_d label,.search-results_f_o label,.search-results_p_d label,.search-results_p_o label,.search-results label{margin-bottom:0;font-size:14px}.search-results .input-group-prepend,.search-results_f_d .input-group-prepend,.search-results_f_o .input-group-prepend,.search-results_p_d .input-group-prepend,.search-results_p_o .input-group-prepend{margin-right:-1px}}.search-results .mobile-scroll,.search-results_f_d .mobile-scroll,.search-results_f_o .mobile-scroll,.search-results_p_d .mobile-scroll,.search-results_p_o .mobile-scroll{width:100%}.search-results .row,.search-results_f_d .row,.search-results_f_o .row,.search-results_p_d .row,.search-results_p_o .row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.search-results .col-12,.search-results_f_d .col-12,.search-results_f_o .col-12,.search-results_p_d .col-12,.search-results_p_o .col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.search-results .pb-2,.search-results .py-2,.search-results_f_d .pb-2,.search-results_f_d .py-2,.search-results_f_o .pb-2,.search-results_f_o .py-2,.search-results_p_d .pb-2,.search-results_p_d .py-2,.search-results_p_o .pb-2,.search-results_p_o .py-2{padding-bottom:.5rem!important}.search-results .mb-4,.search-results .my-4,.search-results_f_d .mb-4,.search-results_f_d .my-4,.search-results_f_o .mb-4,.search-results_f_o .my-4,.search-results_p_d .mb-4,.search-results_p_d .my-4,.search-results_p_o .mb-4,.search-results_p_o .my-4{margin-bottom:1.5rem!important}.search-results .shadow,.search-results_f_d .shadow,.search-results_f_o .shadow,.search-results_p_d .shadow,.search-results_p_o .shadow{box-shadow:0 .5rem 1rem rgba(0,0,0,.15)!important}.search-results .list-group,.search-results_f_d .list-group,.search-results_f_o .list-group,.search-results_p_d .list-group,.search-results_p_o .list-group{display:-ms-flexbox;display:flex;-ms-flex-direction:column;flex-direction:column;padding-left:0;margin-bottom:0;border-radius:.25rem;background-color:#fff}.search-results .list-group-item-action,.search-results_f_d .list-group-item-action,.search-results_f_o .list-group-item-action,.search-results_p_d .list-group-item-action,.search-results_p_o .list-group-item-action{display:-ms-flexbox;display:flex;border:none;-ms-flex-pack:justify;justify-content:space-between}.search-results .list-group-item:first-child,.search-results_f_d .list-group-item:first-child,.search-results_f_o .list-group-item:first-child,.search-results_p_d .list-group-item:first-child,.search-results_p_o .list-group-item:first-child{border-top-left-radius:inherit;border-top-right-radius:inherit}.search-results .list-group-item-action:hover,.search-results_f_d .list-group-item-action:hover,.search-results_f_o .list-group-item-action:hover,.search-results_p_d .list-group-item-action:hover,.search-results_p_o .list-group-item-action:hover{color:#fff;background-color:#5c469c!important;cursor:pointer}.search-results .list-group-item-hover,.search-results_f_d .list-group-item-hover,.search-results_f_o .list-group-item-hover,.search-results_p_d .list-group-item-hover,.search-results_p_o .list-group-item-hover{color:#fff;background-color:#5c469c}.search-results .list-group-item,.search-results_f_d .list-group-item,.search-results_f_o .list-group-item,.search-results_p_d .list-group-item,.search-results_p_o .list-group-item{position:relative;padding:.75rem 1.25rem;background-color:#fff;border:1px solid rgba(0,0,0,.125)}.search-results .list-group-item-action,.search-results_f_d .list-group-item-action,.search-results_f_o .list-group-item-action,.search-results_p_d .list-group-item-action,.search-results_p_o .list-group-item-action{width:100%;color:#495057;text-align:inherit}.search-results .autocomplete-box,.search-results_f_d .autocomplete-box,.search-results_f_o .autocomplete-box,.search-results_p_d .autocomplete-box,.search-results_p_o .autocomplete-box{display:-ms-flexbox;display:flex}.search-results .autocomplete-name-icon,.search-results_f_d .autocomplete-name-icon,.search-results_f_o .autocomplete-name-icon,.search-results_p_d .autocomplete-name-icon,.search-results_p_o .autocomplete-name-icon{height:25px;width:25px}.search-results .autocomplete-name-data,.search-results_f_d .autocomplete-name-data,.search-results_f_o .autocomplete-name-data,.search-results_p_d .autocomplete-name-data,.search-results_p_o .autocomplete-name-data{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;max-width:500px}.search-results .autocomplete-counter,.search-results_f_d .autocomplete-counter,.search-results_f_o .autocomplete-counter,.search-results_p_d .autocomplete-counter,.search-results_p_o .autocomplete-counter{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}@media (min-width:768px){.search-results .d-md-block,.search-results_f_d .d-md-block,.search-results_f_o .d-md-block,.search-results_p_d .d-md-block,.search-results_p_o .d-md-block{display:block!important}}.has-danger{border-bottom:2px solid #dc3545!important}.invalid-feedback{width:100%;margin-top:.25rem;font-size:80%;color:#dc3545;text-align:center}.is-invalid{border-color:#dc3545}



     .b-btn-primary {
    padding-top: 12px;
    padding-bottom: 12px;
}
.bpt-tab button, .bpt-tab a {
    border-radius: .25rem .25rem 0 0;
        margin-right: 3px;
    padding: 18px 15px;
    transition: .2s ease-in-out;
}
.bpt-tabcontent {
    border: none;
    box-shadow: none !important;
}
.bpt-tab button:hover, .bpt-tab a:hover {
    background-color: #fff;
    color: #51408e;
    transition: 1s;
}

.wmk-bc-header .wmk-bc-img h1 {
    color: #fff;
    font-size: 30px;
    font-weight: 700;
    margin: 20px 0 10px;
}
.wmk-bc-header .wmk-bc-img .sp-03 span {
    display: block;
}
.autocomplete-name-data-algolia em {
    font-style: normal;
    font-weight: 700;
}

.input-group-addon input[type="radio"]{
    display:none;
}
.input-group-addon label {
    position: relative;
    cursor: pointer;
    display: inline-flex;
    gap: 0.5em;
    align-items: center;
    padding: 0.5em 0.5em;
}
.input-group-addon label:before{
    content:'';
    height: 1em;
    width: 1em;
    border: 1px solid #0000ff;
    border-radius: 50%;
}

.input-group-addon input[type="radio"]:checked + label:before{
 height: 1em;
 width: 1em;
 border: 5px solid #5c469c;

 background-color:#ffffff ;   
}

.input-group-addon input[type="radio"]:checked + label{
    backgroud-color: #5c469c;
    color:#000000;
    
}

.espacio-rbfligth{
  margin-top:0.5rem !important;
}
.wmk-bc-header{
      background-size: cover;
      background-repeat: no-repeat;
    }
    .lightpick__close-action, .lightpick__next-action, .lightpick__previous-action {
    margin-left: 0;
    margin-right: 5px;
}
.lightpick__next-action, .lightpick__previous-action {
    background-color: #51408e;
    color: #fff;
}
.lightpick__toolbar {
    position: absolute;
    width: 100%;
    display: -ms-flexbox;
    display: flex;
    text-align: right;
    -ms-flex-pack: end;
    padding: 10px 10px 0 10px;
    justify-content: space-between
}

.lightpick__month-title-bar {
    margin: 6px 0 16px 0;
}
.lightpick__month-title {
    margin-left: 40px !important;
    font-size: 14px !important;
}
.lightpick__month-title {
    margin-top: 4px;
    margin-bottom: 4px;
    margin-left: 4px;
    font-size: 16px;
    font-weight: 400;
    line-height: 24px;
    cursor: default;
    padding: 0 4px;
    border-radius: 4px;
    width:60%;
    display: flex;
}
.lightpick__month-title>.lightpick__select-months {
    font-weight: 700;
    font-size: 0.99em;
    margin-right: .5em;
    padding: 1px;
    text-align: right;
}
.lightpick__month-title>.lightpick__select-years {
    margin-right: .5em;
    padding: 1px;
    text-align: left;
}

.lightpick__select-years{
  width: 60%;
}
.lightpick__select-months{
  width: 95%;
}
/*424  contdawn nuevo */
body {
    
    font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
}

/* -------------------CONTADOR------------------- */
#countdown {
    --banner-accent: #A6240A;

    width: 100%;
    max-width: 400px;
    margin: auto;
    padding: 8px 16px;
    display: flex;
    align-items: stretch;
    justify-content: space-evenly;
    gap: 4px;
    background-color: #0A090D50;
    border-radius: 16px;
    text-align: center;
    font-size: 0.75rem;
    color: #fff;
}
#countdown div:not(.separator) {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    row-gap: 4px;
}
#countdown img {
    height: auto;
    width: 100%;
    max-height: 50px;
    min-width: 50px;
    object-fit: contain;
}
#countdown .separator {
    align-self: center;
    width: 1px;
    min-height: 60px;
    max-height: 80px;
    background: var(--banner-accent);
}
#countdown #days, #countdown #hours, #countdown #minutes, #countdown #seconds {
    font-size: 1.5rem;
    font-weight: 600;
}

@media (375px <= width) {
    #countdown div:not(.separator) {
        min-width: 50px;
    }
}
@media (1280px <= width) {
    #countdown {
        font-size: 0.875rem;
        max-width: 548px;
        margin-top:20px;
      /* margin-right: 270px !important;*/

    }
    #countdown div:not(.separator) {
        min-width: 80px;
    }
    #countdown img {
        max-height: 80px;
        min-width: none;
    }
    #countdown #days, #countdown #hours, #countdown #minutes, #countdown #seconds {
        font-size: 2.25rem;
    }
}   

</style>
  
<!--<div class="wmk-bc-header" style="background-image: url(<?php echo $settings['bg']['url'];?>)">-->
<div class="wmk-bc-header c-general" style="padding-left: 5px !important; padding-right: 5px !important; padding-bottom: 15px; background-image: url(<?php echo $settings['bg']['url'];?>);">
   
<div class="b-container p-order-1">
          <div class="b-row wmk-bc-img">
              
                <div class="b-col-12 b-col-md-6">
                    <div class="b-row">
                        <div class="b-col-6">
                            <h4 id="lbl_titulo"><?php echo $settings['titulo']; ?></h4>
                        </div>
                        <div class="b-col-6">
                            <div class="sp-03" id="fechas_busqueda">
                              <h5>
                            <span id="lbl_inicio"></span>
                            <span id="lbl_fin"></span>
                              </h5>
                          </div> 
                        </div>
                    </div>
                    
                    <div class="c-sp">
                    
                        <span class="sp-01"><h4><?php echo $settings['title_header'] ?></h4></span>
                        <h1><span id="lbl_precio" class="sp-02"></span></h1>
                        <?php  if(!empty($settings['second_title'])){ ?>
                        <p style="font-size:20px"><h5><?php echo $settings['second_title'] ?></h5></p>
                        <?php
                        }else{
                        ?>
                        <p style="font-size:11px;text-align:left;"><h7>Impuestos no incluidos.</h7></p>
                        <?php 
                        }
                        ?>
                      </div>                     
                </div>
                <div class="b-col-12 b-col-md-6">
                    <h5 class="s-h5">
                        
                    </h5>
             
                  
        </div>
    </div>
    <!--<div class="wmk-bc-i-mask">            
        <img class="wmk-img-bg" src="<?php //echo $settings['bg']['url']; ?>"/>
    </div>-->
    <div class="b-container p-order-3 mt-3">
        <div class="bpt-container">
        
    <div class="bpt-tab">
      <button  <?php echo $settings['opcion_oferta'][0]=='0' ? 'class="bpt-tablinks bpt-active"' : 'class="bpt-tablinks"'?> onclick="openCity(event, 'bpt-hoteles')">Hoteles</button>
      <button  <?php echo $settings['opcion_oferta'][0]=='2' ? 'class="bpt-tablinks bpt-active"' : 'class="bpt-tablinks"'?> onclick="openCity(event, 'bpt-paquetes')">Hotel + Vuelo</button>

      <!--    <button  <?php /*echo $settings['opcion_oferta'][0]=='1' ? 'class="bpt-tablinks bpt-active"' : 'class="bpt-tablinks"'*/?> onclick="openCity(event, 'bpt-vuelos')">Vuelos</button>
      <a href="https://disney.pricetravel.com/" target="_blank" class="bpt-tablinks">Disney</a>-->
    </div>

    <!-- start hotel -->
    <div id="bpt-hoteles" <?php echo $settings['opcion_oferta'][0]=='0' ? 'class="bpt-tabcontent bpt-d-block"' : 'class="bpt-tabcontent"';?> >
      <div class="b-row">
        <div class="b-col-12 b-col-md-4">
          <div class="b-form" id="bformHotel">
            <label class="b-label">Destino, hotel, punto de interés.</label>
            <input type="text" id="place_name_hotel" class="b-input" autocomplete="off"
              placeholder="Destino, hotel, punto de interés." onclick="this.setSelectionRange(0, this.value.length)" />
            <span class="b-icon material-icons">place</span>
            <div class="search-results hide">
              <div class="row">
                <div class="mobile-scroll">
                  <div class="col-12 mobile-element">
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-12 mobile-header mb-4 pb-2">
                          <div class="d-flex justify-content-between align-items-center mt-3">
                            <p class="mobile-title h5">¿A dónde quieres viajar?</p>
                            <span id="closeBtn" class="b-icon material-icons" style="position: revert;">close</span>
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="b-form">
                            <label class="b-label" for="place_name_hotel_modal"> Destino, hotel, punto de interés.
                            </label>
                            <input type="text" id="place_name_hotelMobile" name="place_name_hotel_modal" class="b-input"
                              placeholder="" onclick="this.setSelectionRange(0, this.value.length)">
                            <span class="b-icon material-icons">place</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-12 mobile-list">
                    <div class="list-group" id="list_box_hotel">

                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="b-col-6 b-col-md-2">
          <div class="b-form bf-01">
            <label class="b-label">LLegada</label>
            <input type="text" class="b-input datepicker1" id="datepicker1" lang="es-MX"/>
            <span class="b-icon material-icons">today</span>
          </div>
        </div>
        <div class="b-col-6 b-col-md-2">
          <div class="b-form bf-01" id="parentBackHotel">
            <label class="b-label">Salida</label>
            <input type="text" class="b-input datepicker2" id="datepicker2"/>
            <span class="b-icon material-icons">event</span>
          </div>
        </div>
        <div class="b-col-12 b-col-md-2">
          <div class="b-form">
            <label class="b-label">Huéspedes</label>
            <input id="inputDropdownHotel" type="text" class="b-input" placeholder="2 Adultos, 1 Habitación" readonly />
            <span class="b-icon material-icons">people</span>

            <!-- start dropdown -->
            <div id="cDropdownHotel" class="bpt-dropdown b-shadow b-d-none">

              <div id="listRooms1">

              </div>


              <div class="b-row">
                <button id="addRoomHotel" class="b-simple-btn">Agregar habitación</button>
              </div>

              <div class="b-row">
                <button id="cApplyHotel" class="b-btn b-btn-primary">Aplicar</button>
              </div>

            </div>
          </div>
        </div>
        <div class="b-col-12 b-col-md-2">
          <button id="searchBtnHotel" class="b-btn b-btn-primary">Buscar</button>
        </div>
      </div>
    </div>

    <!-- start flights -->
<!--    <div id="bpt-vuelos" class="bpt-tabcontent">-->
<!--check box-->
<!--
<div class="b-row" style="height: 30px;">
<div class="b-col-12 b-col-md-6 espacio-rbfligth">
        <div class="input-group" style="padding: 0px 0px 0px 0px;">
          <span class="input-group-addon">
                <input type="radio" class="tripopactive" name="flight-mode" id="flight-mode-roundtrip" value="true" checked>
                <label for="flight-mode-roundtrip"> Redondo </label>
          </span>
          <span class="input-group-addon">
              
                <input type="radio" class="" name="flight-mode" id="flight-mode-oneway" value="false">
                <label for="flight-mode-oneway"> Sencillo  </label>
          </span>
        </div>
</div>
</div>
-->
<!--fin check box
    
    
      <div class="b-row">
        <div class="b-col-12 b-col-md-2">
          <div class="b-form" id="bformFlight">
            <label class="b-label">Origen</label>
            <input type="text" class="b-input" placeholder="Origen" id="place_name_flight_o" />
            <span class="b-icon material-icons">flight_takeoff</span>
            <div class="search-results_f_o hide">
              <div class="row">
                <div class="mobile-scroll">
                  <div class="col-12 mobile-element">
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-12 mobile-header mb-4 pb-2">
                          <div class="d-flex justify-content-between align-items-center mt-3">
                            <p class="mobile-title h5">¿A dónde quieres viajar?</p>
                            <span id="closeBtn2" class="b-icon material-icons" style="position: revert;">close</span>
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="b-form">
                            <label class="b-label" for="place_name_flight_o_modal"> Origen
                            </label>
                            <input type="text" id="place_name_flight_oMobile" name="place_name_flight_o_modal"
                              class="b-input" placeholder="" onclick="this.setSelectionRange(0, this.value.length)"
                              autocomplete="off">
                            <span class="b-icon material-icons">place</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-12 mobile-list">
                    <div class="list-group" id="list_box_flight">

                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="b-col-12 b-col-md-2">
          <div class="b-form bf-01" id="bformFlight_to">
            <label class="b-label">Destino</label>
            <input type="text" class="b-input" placeholder="Destino" id="place_name_flight_d" autocomplete="off" />
            <span class="b-icon material-icons">flight_land</span>
            <div class="search-results_f_d hide">
              <div class="row">
                <div class="mobile-scroll">
                  <div class="col-12 mobile-element">
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-12 mobile-header mb-4 pb-2">
                          <div class="d-flex justify-content-between align-items-center mt-3">
                            <p class="mobile-title h5">To</p>
                            <span id="closeBtn3" class="b-icon material-icons" style="position: revert;">close</span>
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="b-form">
                            <label class="b-label" for="place_name_flight_d_modal"> Destino, hotel, punto de interés.
                            </label>
                            <input type="text" id="place_name_flight_dMobile" name="place_name_flight_d_modal"
                              autocomplete="off" class="b-input" placeholder=""
                              onclick="this.setSelectionRange(0, this.value.length)">
                            <span class="b-icon material-icons">place</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-12 mobile-list">
                    <div class="list-group" id="list_box_flight2">

                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="b-col-6 b-col-md-2">
          <div class="b-form bf-01">
            <label class="b-label">Llegada</label>
            <input type="text" class="b-input" id="datepicker3" readonly />
            <span class="b-icon material-icons">today</span>
          </div>
        </div>
        <div class="b-col-6 b-col-md-2">
          <div class="b-form bf-01">
            <label class="b-label">Salida</label>
            <input type="text" class="b-input" id="datepicker4" readonly />
            <span class="b-icon material-icons">event</span>
          </div>
        </div>
        <div class="b-col-12 b-col-md-2">
          <div class="b-form">
            <label class="b-label">Pasajeros</label>
            <input id="inputDropdownFlight" type="text" class="b-input" placeholder="2 Adultos, 1 Habitación"
              readonly />
            <span class="b-icon material-icons">people</span>
-->
            <!-- start dropdown -->
<!--            <div id="cDropdownFlight" class="bpt-dropdown b-shadow b-d-none">


              <div id="listRooms2">

              </div>
              <div class="b-row">
                <button id="cApplyFlight" class="b-btn b-btn-primary">Aplicar</button>
              </div>

            </div>
          </div>
        </div>
        <div class="b-col-12 b-col-md-2">
          <button class="b-btn b-btn-primary" id="searchBtnFlight">Buscar</button>
        </div>
      </div>
    </div>
-->
    <!-- start packages -->
    <div id="bpt-paquetes" <?php echo $settings['opcion_oferta'][0]=='2' ? 'class="bpt-tabcontent bpt-d-block"' : 'class="bpt-tabcontent"';?> >
      <div class="b-row">
        <div class="b-col-12 b-col-md-2">
          <div class="b-form" id="bformPackage">
            <label class="b-label">Origen</label>
            <input id="place_name_package_o" type="text" class="b-input" placeholder="Origen" autocomplete="off" />
            <span class="b-icon material-icons">flight_takeoff</span>
            <div class="search-results_p_o hide">
              <div class="row">
                <div class="mobile-scroll">
                  <div class="col-12 mobile-element">
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-12 mobile-header mb-4 pb-2">
                          <div class="d-flex justify-content-between align-items-center mt-3">
                            <p class="mobile-title h5">Where do you want to travel?</p>
                            <span id="closeBtn4" class="b-icon material-icons" style="position: revert;">close</span>
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="b-form">
                            <label class="b-label" for="place_name_package_o_modal"> Origen
                            </label>
                            <input type="text" id="place_name_package_oMobile" name="place_name_package_o_modal"
                              class="b-input" placeholder="" onclick="this.setSelectionRange(0, this.value.length)"
                              autocomplete="off">
                            <span class="b-icon material-icons">place</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-12 mobile-list">
                    <div class="list-group" id="list_box_package">

                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="b-col-12 b-col-md-2">
          <div class="b-form bf-01" id="bformPackage_to">
            <label class="b-label">Destino</label>
            <input id="place_name_package_d" type="text" class="b-input" placeholder="Destino" autocomplete="off" />
            <span class="b-icon material-icons">flight_land</span>
            <div class="search-results_p_d hide">
              <div class="row">
                <div class="mobile-scroll">
                  <div class="col-12 mobile-element">
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-12 mobile-header mb-4 pb-2">
                          <div class="d-flex justify-content-between align-items-center mt-3">
                            <p class="mobile-title h5">Destino</p>
                            <span id="closeBtn5" class="b-icon material-icons" style="position: revert;">close</span>
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="b-form">
                            <label class="b-label" for="place_name_package_d_modal"> Destino
                            </label>
                            <input type="text" id="place_name_package_dMobile" name="place_name_package_d_modal"
                              autocomplete="off" class="b-input" placeholder=""
                              onclick="this.setSelectionRange(0, this.value.length)">
                            <span class="b-icon material-icons">place</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-12 mobile-list">
                    <div class="list-group" id="list_box_package2">

                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="b-col-6 b-col-md-2">
          <div class="b-form bf-01">
            <label class="b-label">Salida</label>
            <input type="text" class="b-input" id="datepicker5" />
            <span class="b-icon material-icons">event</span>
          </div>
        </div>
        <div class="b-col-6 b-col-md-2">
          <div class="b-form bf-01">
            <label class="b-label">Regreso</label>
            <input type="text" class="b-input" id="datepicker6" />
            <span class="b-icon material-icons">event</span>
          </div>
        </div>
        <div class="b-col-12 b-col-md-2">
          <div class="b-form">
            <label class="b-label">Huéspedes </label>
            <input id="inputDropdownPackage" type="text" class="b-input" placeholder="2 Adultos, 1 Habitación"
              readonly />
            <span class="b-icon material-icons">people</span>

            <!-- start dropdown -->
            <div id="cDropdownPackage" class="bpt-dropdown b-shadow b-d-none">

              <div id="listRooms3">

              </div>


              <div class="b-row">
                <button id="addRoomPackage" class="b-simple-btn">Agregar Habitación</button>
              </div>

              <div class="b-row">
                <button id="cApplyPackage" class="b-btn b-btn-primary">Aplicar</button>
              </div>

            </div>
          </div>
        </div>
        <div class="b-col-12 b-col-md-2">
          <button id="searchBtnPackage" class="b-btn b-btn-primary">Buscar</button>
        </div>
      </div>

 <!--   <div id="bpt-disney" class="bpt-tabcontent">
        <h3>Disney</h3>
      </div>
  -->

    </div>

    <div class="c-datepicker"></div>
  </div>


   
    </div>
    
</div>
<script src="<?php echo plugins_url( '/caja_busqueda_dinamica.js?v=16', __FILE__ ) ?>"></script>
<script>
   //var booker = Booker("place_name_hotel","2023-01-24"+":","2023-01-27");
   function isMobile() {
        return /Mobi|Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i.test(navigator.userAgent);
    }
var prueba=<?php echo $settings['contador_desde']; ?>;
var prueba2=<?php echo $settings['contador_hasta']; ?>;
if(prueba!="" && prueba2!="" ){
    if (isMobile()) {
        // Contador Mobile
        var container = document.querySelectorAll('.b-container')[1];
        var newHTML = '<div class=c-contador-mobile><div class=c-countdown id=countdown data-cfrom=<?php echo $settings['contador_desde']; ?> data-hfrom=<?php echo $settings['hora_desde']; ?> data-cto=<?php echo $settings['contador_hasta']; ?> data-hto=<?php echo $settings['hora_hasta']; ?>
        ><div><img alt=logo-campaña height=50px src=<?php echo $settings['logo_campaign']['url'];?> width=50px> <span class=label>Quedan:</span></div><div><span id=days>00</span> <span class=label>Días</span></div><div class=separator></div><div><span id=hours>00</span> <span class=label>Horas</span></div><div class=separator></div><div><span id=minutes>00</span> <span class=label>Minutos</span></div><div class=separator></div><div><span id=seconds>00</span> <span class=label>Segundos</span></div></div></div>';
        container.insertAdjacentHTML('afterend', newHTML);

    } else {
        // Contador Desktop
        var targetDiv = document.querySelectorAll('.wmk-bc-img div')[6];
        targetDiv.innerHTML += '<div class=c-contador><div class=c-countdown id=countdown data-cfrom=<?php echo $settings['contador_desde']; ?> data-hfrom=<?php echo $settings['hora_desde']; ?> data-cto=<?php echo $settings['contador_hasta']; ?> data-hto=<?php echo $settings['hora_hasta']; ?>><div><img alt=logo-campaña height=50px src=<?php echo $settings['logo_campaign']['url'];?> width=50px> <span class=label>Quedan:</span></div><div><span id=days>00</span> <span class=label>Días</span></div><div class=separator></div><div><span id=hours>00</span> <span class=label>Horas</span></div><div class=separator></div><div><span id=minutes>00</span> <span class=label>Minutos</span></div><div class=separator></div><div><span id=seconds>00</span> <span class=label>Segundos</span></div></div>';
    }
  }

   // Configura la fecha objetivo para la cuenta regresiva
const countdownElement = document.getElementById('countdown');

// Obtiene los valores de los atributos data-cfrom y data-cto
const cFrom = countdownElement.dataset.cfrom;
const hFrom = countdownElement.dataset.hfrom;

const cTo = countdownElement.dataset.cto;
const hTo = countdownElement.dataset.hto;

// Concatenar las fechas y horas
const dateFromString = `${cFrom}T${hFrom}:00`;
const dateToString = `${cTo}T${hTo}:00`;

// Crear objetos Date
const dateFrom = new Date(dateFromString);
const dateTo = new Date(dateToString);

// Convertir a la zona horaria de Ciudad de México
const options = { timeZone: 'America/Mexico_City', hour12: false, year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' };

const fromDate = new Date(dateFrom.toLocaleString('en-US', options));
const targetDate = new Date(dateTo.toLocaleString('en-US', options));


const countdown = setInterval(function() {
    const now = new Date().getTime();
    const timeRemaining = targetDate - now;

    // Calcula días, horas, minutos y segundos restantes
    const days = Math.floor(timeRemaining / (1000 * 60 * 60 * 24));
    const hours = Math.floor((timeRemaining % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((timeRemaining % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((timeRemaining % (1000 * 60)) / 1000);

    // Actualiza el contenido de los elementos HTML
    document.getElementById("days").innerText = days < 10 ? "0" + days : days;
    document.getElementById("hours").innerText = hours < 10 ? "0" + hours : hours;
    document.getElementById("minutes").innerText = minutes < 10 ? "0" + minutes : minutes;
    document.getElementById("seconds").innerText = seconds < 10 ? "0" + seconds : seconds;

    // Si la cuenta regresiva ha terminado
    if (timeRemaining < 0) {
        clearInterval(countdown);
       // document.getElementById("countdown").innerHTML = "¡Evento finalizado!";
        document.getElementById("countdown").remove()
    }
}, 1000);
</script>
  <?php
  }

}